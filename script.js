class Employee {
     constructor(name, age, salary) {
          this._name = name;
          this._age = age;
          this._salary = salary;
     }
     get name() {
          return this._name;
     }
     set name(newName) {
          this._name = newName;
     }
     get age() {
          return this._age;
     }
     set age(newAge) {
          this._name = newAge;
     }
     get salary() {
          return this._salary;
     }
     set salary(newSalary) {
          this._salary = newSalary;
     }
}

class Programmer extends Employee {
     constructor(name, age, salary, lang) {
          super(name, age, salary);
          this._lang = lang;
     }
     get salary() {
          return this._salary * 3;
     }
     get lang() {
          return this._lang = lang;
     }
     set lang(newLang) {
          this._lang = newLang;
     }
}

let john = new Programmer('Jhon', 25, 3000, 'JS');
let maxim = new Programmer('Maxim', 27, 4000, 'Pyton');
console.log(john);
console.log(maxim);